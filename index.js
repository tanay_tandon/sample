/** @format */

import {AppRegistry} from 'react-native';
import {LookupWrapper} from './demo/components/LookupWrapper';
import {name as appName} from './app.json';

import React from 'react';

import {Provider} from 'react-redux';
import configureStore from "./store";

const store = configureStore();

const RNRedux = () => (
    <Provider store={store}>
        <LookupWrapper/>
    </Provider>
);


AppRegistry.registerComponent(appName, () => RNRedux);
