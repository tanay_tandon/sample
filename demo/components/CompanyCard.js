import React, {Component} from 'react';

import {View, StyleSheet} from 'react-native';

type props = {};

import {LinkButton, SearchBox} from '../../components';
import {LabelledText} from "../../components/LabelledText";

export class CompanyCard extends Component<Props> {

    render() {
        return (
            <View>
                <LabelledText label="Company Name" value={this.props.company.name}/>
                <LabelledText label="CEO Name" value={this.props.company.ceo}/>
                <LabelledText label="Price" value={this.props.company.price}/>
                <LinkButton url={this.props.company.website} value="Goto Website"/>
            </View>
        )
    }

}