import React, {Component} from 'react';

type props = {};

import {connect} from 'react-redux';

import {View, StyleSheet, Text, ProgressBarAndroid as ProgressBar} from 'react-native';
import {SearchBox} from '../../components';

import {CompanyCard} from './CompanyCard';
import {companyFound, companyNotFound, lookingUpCompany} from "../action";

import Fint from '../../Fint';
import * as constants from '../constants';

class Lookup extends Component<Props> {

    state = {symbol: ''};

    onSearchPressed = (text) => {
        this.props.lookup();
        this.setState({
            symbol: text
        });
        Fint.lookUp(text, (name, website, ceo, price) => {
            this.props.companyFound({name, website, ceo, price});
        }, (statusCode, message) => {
            this.props.companyNotFound();
        });
    };

    container = () => {
        switch (this.props.data.status) {
            case constants.FOUND: {
                return (<CompanyCard company={this.props.data.company}/>);
            }


            case constants.INITIAL_STATE: {
                return <Text>Enter the symbol to lookup company data</Text>
            }
            case constants.NOT_FOUND: {
                return <Text>No stock found for symbol {this.state.symbol}</Text>
            }
            case constants.LOADING: {
                return <ProgressBar styleAttr="Large"/>;
            }
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <SearchBox disabled={this.props.data.status === constants.LOADING} style={styles.inputContainer}
                           placeholder="Enter company symbol" buttonText="Search"
                           onSearchPressed={this.onSearchPressed}/>
                {this.container()}
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        paddingTop: 30,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    }
});

const mapStateToProps = state => {
    return {
        data: state.lookup
    }
};

const mapDispatchToProps = dispatch => {
    return {
        lookup: () => {
            dispatch(lookingUpCompany())
        },
        companyFound: (company) => {
            dispatch(companyFound(company))
        },
        companyNotFound: () => {
            dispatch(companyNotFound());
        }
    }
};

export const LookupWrapper = connect(mapStateToProps, mapDispatchToProps)(Lookup);