import {COMPANY_NOT_FOUND, COMPANY_FOUND, LOOKING_UP_COMPANY} from './type';

import * as constants from './constants';

const initialState = {
    status: constants.INITIAL_STATE,
    company: {}
};

export const companyLookup = (state = initialState, action) => {
    switch (action.type) {
        case COMPANY_FOUND: {
            return {
                ...state,
                company: action.payload,
                status: constants.FOUND
            }
        }

        case COMPANY_NOT_FOUND: {
            return {
                ...state,
                company: {},
                status: constants.NOT_FOUND
            }
        }

        case LOOKING_UP_COMPANY: {
            return {
                ...state,
                company: {},
                status: constants.LOADING
            }
        }

        default:
            return state
    }
};

