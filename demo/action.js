import {COMPANY_FOUND, COMPANY_NOT_FOUND, LOOKING_UP_COMPANY} from './type';

export const companyFound = company => {

    return {
        type: COMPANY_FOUND,
        payload: company
    }

};

export const companyNotFound = () => {
    return {
        type: COMPANY_NOT_FOUND
    }
};

export const lookingUpCompany = () => {
    return {
        type: LOOKING_UP_COMPANY
    }
};