import React, {Component} from 'react';
import {Text} from 'react-native';

type props = {};

export class LabelledText extends Component<Props> {

    getLabelledText = () => {
        let value = this.props.value + '';
        return this.props.label + ":" + (value.trim() === '' ? 'NA' : value.trim());
    };

    render() {
        return <Text>{this.getLabelledText()}</Text>;
    }

}
