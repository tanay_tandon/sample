import React, {Component} from 'react';

import {Button, Linking} from 'react-native';

type props = {};

export class LinkButton extends Component<Props> {


    openLink = () => {
        Linking.openURL(this.props.url);
    };


    render() {
        return (
            <Button title={this.props.value} onPress={this.openLink}/>
        );
    }

}