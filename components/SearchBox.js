import React, {Component} from 'react';
import {StyleSheet, View, TextInput, Button} from 'react-native';

type props = {};

export class SearchBox extends Component<Props> {

    state = {
        searchText: ''
    };

    onTextChanged = (value) => {
        this.setState({
            searchText: value
        });
    };

    search = () => {
        if (this.state.searchText.trim() === '')
            return;
        this.props.onSearchPressed(this.state.searchText);
    };

    render() {
        return (<View style={styles.inputContainer}>
            <TextInput placeholder={this.props.placeholder} onChangeText={this.onTextChanged}
                       value={this.state.searchText} style={styles.searchInput}/>
            <Button title={this.props.buttonText} disabled={this.props.disabled} onPress={this.search} style={styles.searchButton}/>
        </View>);
    }

}

const styles = StyleSheet.create({
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },
    searchInput: {
        width: '70%'
    },
    searchButton: {
        width: '30%'
    }
});
