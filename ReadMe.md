# Sample app

A React Native Android app that interacts with the [Fint Library](https://bitbucket.org/tanay_tandon/fint/src/master/)
and allows the user to lookup the information about a stock (company name, CEO name, Link to website, and Current Stock Price)

#Requirements

NPM version 6.4.1,
Java 8,
Node 10.13.0,
Watchman 4.9.0


# How to use

`git clone git@bitbucket.org:tanay_tandon/sample.git`

`cd sample`

`npm install `

`rm -rf fint`

`git clone git@bitbucket.org:tanay_tandon/fint.git`

`react-native run-android`