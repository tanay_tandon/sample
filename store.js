import {createStore, combineReducers} from 'redux';

import {companyLookup as lookup} from './demo/reducer';

const rootReducer = combineReducers({
    lookup: lookup
});

const configureStore = () => {
    return createStore(rootReducer);
};

export default configureStore;