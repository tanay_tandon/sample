package com.sample;

import android.util.Log;
import android.widget.Toast;

import com.example.tanaytandon.fint.FintCallback;
import com.example.tanaytandon.fint.internal.Fint;
import com.example.tanaytandon.fint.models.CInfo;
import com.example.tanaytandon.fint.models.NetworkError;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class FintModule extends ReactContextBaseJavaModule {


    public static final String TAG = "Random";

    public FintModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "Fint";
    }

    @ReactMethod
    public void lookUp(String symbol, final Callback onSuccess, final Callback onFailure) {
        Fint.with(getReactApplicationContext()).search(symbol, new FintCallback<CInfo>() {
            @Override
            public void onSuccess(CInfo result) {
                onSuccess.invoke(result.getCompany().getName(), result.getCompany().getWebsite(), result.getCompany().getCEO(), result.getPrice());
            }

            @Override
            public void onFail(NetworkError networkError) {
                onFailure.invoke(networkError.getStatusCode(), networkError.getMessage());
            }

        });
    }
}
